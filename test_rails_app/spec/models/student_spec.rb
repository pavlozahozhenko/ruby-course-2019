require 'rails_helper'

RSpec.describe Student, type: :model do
  context '#is_enought_credits?' do
    subject { ::FactoryBot.create :student }

    it 'should return true if the student has more than 239 credits' do
      subject.credits = 240
      expect(subject.is_enough_credits?).to be true
    end

    it 'should return false if the student has 0 credits' do
      subject.credits = 0
      expect(subject.is_enough_credits?).to be false
    end

    it 'should return false if student has 30 credits' do
      subject.credits = 30
      expect(subject.is_enough_credits?).to be false
    end
  end
end
