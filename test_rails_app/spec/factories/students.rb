FactoryBot.define do
  factory :student do
    first_name { ::Faker::Name.first_name }
    last_name { ::Faker::Name.last_name }
    group
    avg_rating { 61 + rand(30) }
  end
end
