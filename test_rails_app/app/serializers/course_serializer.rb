class CourseSerializer < ActiveModel::Serializer
  attributes :name, :course_year, :is_useful?

  def is_useful?
    # do something
    object.name == 'Ruby'
  end

  def course_year
    object.year
  end

  def include_course_year?
    object.year.present?
  end
end
