Feature: Main flow

  Background:
    Given user exists
    Given I am on login page
    When I fill in "user[email]" field with "test@example.com"
    And I fill in "user[password]" field with "password"
    And I click on "Log in"

  @javascript
  Scenario: User logs into the system
    Then I should see text "Students list"

  @javascript
  Scenario: Admin user logs into the system and adds a new course
    Given user is admin
    When I go to courses
    And I wait for 3 seconds
    And I click on "New Course"
    When I fill in "course[name]" field with "Test Course"
    And I click on "Create"
    And I wait for 2 seconds
    And I click on "Back"
    And I wait for 2 seconds
    Then there should be a course with name "Test Course"
